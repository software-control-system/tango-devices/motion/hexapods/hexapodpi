/**
 * Device Class Identification:
 * 
 * Class Name   :	HexapodPI
 * Contact      :	jean.coquet@synchrotron-soleil.fr
 * Class Family :	Motion
 * Platform     :	All Platforms
 * Bus          :	Ethernet
 * Manufacturer :	PI (Physik Instrument)
 * Reference    :	PI M810
 */
