#include "PIHexapod.h"

#include <iostream>
#include <numeric>

using namespace std;

#ifndef _MSC_VER
#define __FUNCTION__ __func__
#endif


// ============================================================================
// Config::Config
// ============================================================================
PIHexapod::Config::Config ()
{
  url                 = "Not Initialised";
  port = 50000;
  version810 = true;
}

PIHexapod::Config::Config (const Config & _src)
{
  *this = _src;
}
// ============================================================================
// DBConfig::operator =
// ============================================================================
void PIHexapod::Config::operator = (const Config & _src)
{
  url                 = _src.url;
  port                = _src.port;
  version810          = _src.version810;
}


// ============================================================================
// CTor
// ============================================================================
PIHexapod::PIHexapod(Config & _conf)
: m_State(NOT_CONNECTED),
conf (_conf),
m_velocity (0)
{
	lastTarget = std::vector<double>(6, 0.0);
  try 
  {
	  this->Disconnect ();

    this->Connect (conf);
	  this->InitSocket ();
    this->m_State = CONNECTED;
  }
  catch (const yat::SocketException & e)
  {
    this->m_State = CONNECTION_ERROR;
    return;
  } 

}

// ============================================================================
// DTor
// ============================================================================
PIHexapod::~PIHexapod ()
{
	this->Disconnect();
}

// ============================================================================
// Init Socket Options
// ============================================================================
void PIHexapod::InitSocket()
{


	m_Socket.set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
	m_Socket.set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
	m_Socket.set_option(yat::Socket::SOCK_OPT_OTIMEOUT, 10000);
	m_Socket.set_option(yat::Socket::SOCK_OPT_ITIMEOUT, 10000);

}

// ============================================================================
// Socket connect
// ============================================================================
void PIHexapod::Connect(Config conf)
{
	yat::Socket::init();

  yat::Address addr(conf.url, conf.port);
	m_Socket.connect(addr);

	m_Socket << "*IDN?\n";
	string answer;
	m_Socket >> answer;
	cout << "hexapod connected to \"" << answer << "\"" << endl;

	m_State = ReadStateFromHexapod();

}
// ============================================================================
// Socket Disconnect
// ============================================================================

void PIHexapod::Disconnect()
{
	//m_Socket.disconnect();
	m_State = NOT_CONNECTED;

}
// ============================================================================
// Find out State of Hexapod - internal utility method
// ============================================================================
PIHexapod::HEXAPOD_STATE PIHexapod::ReadStateFromHexapod811()
{
  YAT_TRACE("PIHexapod::ReadStateFromHexapod");
  //- protect against socket errors
  if (m_State == NOT_CONNECTED || m_State == CONNECTION_ERROR)
  {
    YAT_LOG("PIHexapod::ReadStateFromHexapod() NOT CONNECTED OR CONNECTION_ERROR"); 
    return m_State;
  }
  //Read Motion status
	char c = 5;
	m_Socket.send(&c, 1);
	string answer;
	int bitmask = 0;
	m_Socket >> answer;

	stringstream ss;
	ss << std::hex <<answer;
	ss >> bitmask;

	if ( (bitmask & HEXAPOD_MOTION) != 0 )
	{
		return MOVING;
	}

  //Read System Status
  c = 4;
	m_Socket.send(&c, 1);
	answer.clear();
	bitmask = 0;
	m_Socket >> answer;

	ss.str("");
	ss << std::hex <<answer;
	ss >> bitmask;	

	if ( (bitmask & HEXAPOD_READY) == 0 )
	{
		return NOT_REFERENCED;
	}

	if ( (bitmask & HEXAPOD_REFERENCING) != 0 )
	{
		return REFERENCING;
	}		

	if ( (bitmask & HEXAPOD_MOTION_ERROR) != 0 )
	{
		return MOTION_ERROR;
	}

	return STANDBY;
}

// ============================================================================
// Find out State of Hexapod - internal utility method
// ============================================================================
PIHexapod::HEXAPOD_STATE PIHexapod::ReadStateFromHexapod()
{
  YAT_TRACE("PIHexapod::ReadStateFromHexapod");
  //- protect against socket errors
  if (m_State == NOT_CONNECTED || m_State == CONNECTION_ERROR)
  {
    YAT_LOG("PIHexapod::ReadStateFromHexapod() NOT CONNECTED OR CONNECTION_ERROR"); 
    return m_State;
  }
  // version 811
	if (!conf.version810)
		return ReadStateFromHexapod811();

  //Version 810
	char c = 4;
	m_Socket.send(&c, 1);
	string answer;
	int bitmask = 0;
	m_Socket >> answer;


	istringstream is(answer);
	is >> bitmask;

	if ( (bitmask & HEXAPOD_READY) == 0 )
	{
		return NOT_REFERENCED;
	}

	if ( (bitmask & HEXAPOD_REFERENCING) != 0 )
	{
		return REFERENCING;
	}		

	if ( (bitmask & HEXAPOD_MOVING) != 0 )
	{
		return MOVING;
	}	
	if ( (bitmask & HEXAPOD_MOTION_ERROR) != 0 )
	{
		return MOTION_ERROR;
	}

	return STANDBY;
	
}

// ============================================================================
// Get state of hexapod
// ============================================================================
PIHexapod::HEXAPOD_STATE PIHexapod::GetStatus()
{
  YAT_TRACE("PIHexapod::GetStatus");
	if (m_State == MOVING || m_State == REFERENCING)
	{
		m_State = ReadStateFromHexapod();
	}

	return m_State;
}

// ============================================================================
// Command Stop
// ============================================================================
void PIHexapod::Stop()
{
	CheckConnection(__FUNCTION__);
	char c = 24;
	m_Socket.send(&c, 1);
	ReadError();
}

// ============================================================================
// Get error code
// ============================================================================
int PIHexapod::ReadError()
{
	m_Socket << "ERR?\n";
	string answer;
	int errorcode = 0;
	m_Socket >> answer;
	istringstream is(answer);
	is >> errorcode;
	return errorcode;
}

// ============================================================================
// command Home
// ============================================================================
void PIHexapod::Home()
{
	CheckConnection(__FUNCTION__);
	if (m_State != NOT_REFERENCED)
		CheckBusy(__FUNCTION__);
	if (conf.version810)
	{
		m_Socket << "INI\n";
		m_State = ReadStateFromHexapod();
		if (m_State != REFERENCING)
		{
			ThrowHexapodError("Homing not started", __FUNCTION__);
		}
	}
	else
	{

		m_Socket << "FRF \n";
	  m_State = ReadStateFromHexapod();
		if (m_State != MOVING)
		{
			ThrowHexapodError("Homing not started", __FUNCTION__);
		}
  }
}

// ============================================================================
// Utility : throw exception with hexapod error code
// ============================================================================
void PIHexapod::ThrowHexapodError(const string& description, const string& funcname)
{
	int err = ReadError();
	ostringstream os;
	os << description << " - hexapod reported error " <<err;
	throw yat::Exception( "HEXAPOD_ERROR", os.str(), funcname );

}

// ============================================================================
//
// ============================================================================
void PIHexapod::CheckHexapodForError(const std::string& description, const std::string& funcname)
{
	int err = ReadError();
	if (0 == err)
		return;
	ostringstream os;
	os << description << " - hexapod reported error " <<err;
	throw yat::Exception( "HEXAPOD_ERROR", os.str(), funcname );
}

// ============================================================================
// Utility : throw exception - software error code
// ============================================================================
void PIHexapod::ThrowSoftwareError(const string& description, const string& funcname)
{
	throw yat::Exception( "SOFTWARE_ERROR", description, funcname );
}

// ============================================================================
// command : move (6 axes)
// ============================================================================
void PIHexapod::Move(const std::vector<double>& target)
{
	CheckConnection(__FUNCTION__);
	if (target.size() != 6)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of target positions (must be 6)",
								__FUNCTION__ );
	}
	CheckStandby(__FUNCTION__);
	ostringstream cmd;
	cmd	<< "MOV" 
		<< " X " << target[0]
		<< " Y " << target[1]
		<< " Z " << target[2]
		<< " U " << target[3]
		<< " V " << target[4]
		<< " W " << target[5]
		<<"\n";
	m_Socket << cmd.str();
	
	cout << "HEXAPOD: sent "<<cmd.str();
	m_State = ReadStateFromHexapod();
	if (m_State != MOVING)
	{
		ThrowHexapodError( "Motion not started", __FUNCTION__);
	}
	lastTarget = target;
}

// ============================================================================
// access to 6 positions
// ============================================================================
void PIHexapod::GetPosition(std::vector<double>& positions)
{
	CheckConnection(__FUNCTION__);
	if (m_State != STANDBY)
	{
		m_State = ReadStateFromHexapod();
	}
	if (m_State == STANDBY)
	{
		m_Socket << "POS?\n";
	}
	else
	{
		char c = 3;
		m_Socket.send(&c, 1);
	}
	ReadPosition(positions);
}

// ============================================================================
// read on hexapod the 6 positions in 1 shot
// ============================================================================
void PIHexapod::ReadPosition(std::vector<double>& positions)
{
	positions.clear();
	string answer;
	do
	{
		m_Socket >> answer;
		istringstream is(answer);
		string line;
		double pos;
		while(getline(is, line))
		{
			size_t p = line.find('=');
			if (p == string::npos)
			{
				throw yat::Exception(	"SOFTWARE_ERROR",
										"unexpected response to position query",
										__FUNCTION__ );
			}
			istringstream spos(line.substr(p+1));
			spos >> pos;
			positions.push_back(pos);
		}
	} while (positions.size()<6);
}

// ============================================================================
// Get the Pivot Point Coordinates
// ============================================================================
std::vector <double> PIHexapod::GetPivotPointCoordinates (void)
{
  YAT_TRACE("PIHexapod::GetPivotPointCoordinates");
  //- protect against socket errors
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	std::vector <double> coords;
  m_Socket << "SPI?\n";
	string answer;
	do
	{
		m_Socket >> answer;

		istringstream is(answer);
		string line;
		double pos;
		while(getline(is, line))
		{
			size_t p = line.find('=');
			if (p == string::npos)
			{
				ThrowSoftwareError("unexpected response to pivot coordinate query",	__FUNCTION__ );
			}
			istringstream spos(line.substr(p+1));
			spos >> pos;
			coords.push_back(pos);

		}
	} while (coords.size()<3);

  return coords;
}

// ============================================================================
// Set the Pivot Point Coordinates
// ============================================================================
void PIHexapod::SetPivotPointCoordinates (std::vector <double> pivot_coord)
{
	YAT_TRACE("PIHexapod::SetPivotPointCoordinates");
	//- protect against socket errors
	CheckConnection(__FUNCTION__);
	if (pivot_coord.size () < 3)
	{
		YAT_LOG("PIHexapod::SetPivotPointCoordinates() must provide the 3 positions U,V,W are ignored)"); 
		ThrowSoftwareError ("must provide at least the X,Y,Z positions (U,V,W are ignored)",
			__FUNCTION__);
	}
	CheckStandby(__FUNCTION__);
	ostringstream cmd;
	cmd	<< "SPI" 
		<< " R " << pivot_coord[0]
		<< " S " << pivot_coord[1]
		<< " T " << pivot_coord[2]
		<<"\n";
	m_Socket << cmd.str();
}

// ============================================================================
// Send ASCII command (GCS) to controller must be terminated by LineFeed (\n)
// ============================================================================
void PIHexapod::SendGCSCommand(const std::string& command)
{
	if (m_State == NOT_CONNECTED || m_State == CONNECTION_ERROR)
	{
		YAT_LOG("PIHexapod::SetPivotPointCoordinates() NOT CONNECTED OR CONNECTION_ERROR"); 
		ThrowSoftwareError("Hexapod not connected", __FUNCTION__);
	}
	m_Socket << command;
}

// ============================================================================
// Send ASCII answer (GCS) from controller
// ============================================================================
std::string PIHexapod::ReadGCSAnswer()
{
	CheckConnection(__FUNCTION__);
	std::string answer;
	std::string line;
	// single lines in multiline answers will be terminated with " \n" (Space + LineFeed).
	// Only the last line will end with a linefeed without space.
	for(;;)
	{
		m_Socket >> line;
		answer += line;
		if (	(line.length()==1)
			&&	(line == "\n")		)
		{
			break;
		}
		if (	(line.length()>1)
			&&	(line[line.length()-1] == '\n')
			&&	(line[line.length()-2] != ' ')	)
		{
			break;
		}
	}
	return answer;
}

// ============================================================================
// Get current position of X axis
// ============================================================================
double PIHexapod::GetXPos()
{
	return ReadSinglePosition("X", 0);
}

// ============================================================================
// Get current position of Y axis
// ============================================================================
double PIHexapod::GetYPos()
{
	return ReadSinglePosition("Y", 1);
}

// ============================================================================
// Get current position of Z axis
// ============================================================================
double PIHexapod::GetZPos()
{
	return ReadSinglePosition("Z", 2);
}

// ============================================================================
// Get current position of U axis
// ============================================================================
double PIHexapod::GetUPos()
{
	return ReadSinglePosition("U", 3);
}

// ============================================================================
// Get current position of V axis
// ============================================================================
double PIHexapod::GetVPos()
{
	return ReadSinglePosition("V", 4);
}

// ============================================================================
// Get current position of W axis
// ============================================================================
double PIHexapod::GetWPos()
{
	return ReadSinglePosition("W", 5);
}

// ============================================================================
// utility: read position of singel axis
// ============================================================================
double PIHexapod::ReadSinglePosition(const std::string& axisName, int index)
{
	CheckConnection(__FUNCTION__);
	if (m_State != STANDBY)
	{
		m_State = ReadStateFromHexapod();
	}
	if (m_State != STANDBY)
	{	
		std::vector<double> pos;
		GetPosition(pos);
		return pos[index];
	}
	ostringstream cmd;
	cmd	<< "POS? " << axisName <<"\n";
	m_Socket << cmd.str();
	std::string answer;
	m_Socket >> answer;
	size_t p = answer.find('=');
	if (p == string::npos)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"unexpected response to query",
								__FUNCTION__ );
	}
	istringstream is(answer.substr(p+1));
	double pos;
	is >> pos;
	return pos;
}

// ============================================================================
// set system velocity
// ============================================================================
void PIHexapod::SetVelocity(double vel)
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	ostringstream cmd;

	if (conf.version810)
		cmd	<< "VEL " << vel <<"\n";
	else
		cmd	<< "VLS " << vel <<"\n";

	m_Socket << cmd.str();
	CheckHexapodForError("Could not set velocity", __FUNCTION__);
}

// ============================================================================
// get system velocity
// ============================================================================
double PIHexapod::GetVelocity()
{
	CheckConnection(__FUNCTION__);
	//- CheckStandby(__FUNCTION__);
  if (IsMoving ())
    return m_velocity;

	if (conf.version810)
		m_Socket << "VEL?\n";
	else
		m_Socket << "VLS?\n";

	std::string answer;
	m_Socket >> answer;
	istringstream is(answer);
	//- double vel;
	is >> m_velocity;
	return m_velocity;
}

// ============================================================================
// enable servo control
// ============================================================================
void PIHexapod::EnableServo()
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	if (conf.version810)
	  m_Socket << "SVO 1\n";
	else
		m_Socket << "SVO X 1\n";
	CheckHexapodForError("Could not set ON", __FUNCTION__);
}

// ============================================================================
// disable servo control
// ============================================================================
void PIHexapod::DisableServo()
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	
	if (conf.version810)
	  m_Socket << "SVO 0\n";
	else
		m_Socket << "SVO X 0\n";	
	CheckHexapodForError("Could not set OFF", __FUNCTION__);
}

// ============================================================================
// Get current soft limits
// ============================================================================
void PIHexapod::GetSoftLimits(std::vector<double>& positiveLimits, std::vector<double>& negativeLimits)
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	m_Socket << "PLM?\n";
	ReadPosition(positiveLimits);
	m_Socket << "NLM?\n";
	ReadPosition(negativeLimits);
}

// ============================================================================
// Set soft limits
// ============================================================================
void PIHexapod::SetSoftLimits(const std::vector<double> positiveLimits, const std::vector<double> negativeLimits)
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	ostringstream cmd;
	cmd	<< "PLM" 
		<< " X " << positiveLimits[0]
		<< " Y " << positiveLimits[1]
		<< " Z " << positiveLimits[2]
		<< " U " << positiveLimits[3]
		<< " V " << positiveLimits[4]
		<< " W " << positiveLimits[5]
		<<"\n";
	m_Socket << cmd.str();
	CheckHexapodForError("Could not set positive soft limits", __FUNCTION__);

	cmd	<< "NLM" 
		<< " X " << negativeLimits[0]
		<< " Y " << negativeLimits[1]
		<< " Z " << negativeLimits[2]
		<< " U " << negativeLimits[3]
		<< " V " << negativeLimits[4]
		<< " W " << negativeLimits[5]
		<<"\n";
	m_Socket << cmd.str();
	CheckHexapodForError("Could not set negative soft limits", __FUNCTION__);

}

// ============================================================================
// Enable softlimits
// input: vector with 6 integer values
// if value is !=0 corresponding soft limit will be enabled
// ============================================================================
void PIHexapod::EnableSoftLimits (const std::vector<int>& sslStates)
{
	CheckConnection(__FUNCTION__);
	if (sslStates.size() != 6)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of soft limits states (must be 6)",
								__FUNCTION__ );
	}

	CheckStandby(__FUNCTION__);
	if (std::accumulate(sslStates.begin(), sslStates.end(), 0) == 0)
		return; // Nothing to do
	ostringstream cmd;
	cmd	<< "SSL";

	if (conf.version810)
	{
		if (sslStates[0] != 0)
			cmd << " X 1";
		if (sslStates[1] != 0)
			cmd << " Y 1";
		if (sslStates[2] != 0)
			cmd << " Z 1";
		if (sslStates[3] != 0)
			cmd << " U 1";
		if (sslStates[4] != 0)
			cmd << " V 1";
		if (sslStates[5] != 0)
			cmd << " W 1";
	}
	else
	{
		if (sslStates[0] != 0)
			cmd << " X1";
		if (sslStates[1] != 0)
			cmd << " Y1";
		if (sslStates[2] != 0)
			cmd << " Z1";
		if (sslStates[3] != 0)
			cmd << " U1";
		if (sslStates[4] != 0)
			cmd << " V1";
		if (sslStates[5] != 0)
			cmd << " W1";
	}
	cmd << "\n";
	m_Socket << cmd.str();
	CheckHexapodForError("Could not enable soft limits", __FUNCTION__);

}

// ============================================================================
// Disable softlimits
// input: vector with 6 integer values
// if value is !=0 corresponding soft limit will be disabled
// ============================================================================
void PIHexapod::DisableSoftLimits (const std::vector<int>& sslStates)
{
	CheckConnection(__FUNCTION__);
	if (sslStates.size() != 6)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of soft limits states (must be 6)",
								__FUNCTION__ );
	}

	CheckStandby(__FUNCTION__);
	if (std::accumulate(sslStates.begin(), sslStates.end(), 0) == 0)
		return; // Nothing to do
	ostringstream cmd;
	cmd	<< "SSL";
	//JADE	
	if (true)
	{
		if (sslStates[0] != 0)
			cmd << " X 0";
		if (sslStates[1] != 0)
			cmd << " Y 0";
		if (sslStates[2] != 0)
			cmd << " Z 0";
		if (sslStates[3] != 0)
			cmd << " U 0";
		if (sslStates[4] != 0)
			cmd << " V 0";
		if (sslStates[5] != 0)
			cmd << " W 0";

	}
	else
	{
		if (sslStates[0] != 0)
			cmd << " X0";
		if (sslStates[1] != 0)
			cmd << " Y0";
		if (sslStates[2] != 0)
			cmd << " Z0";
		if (sslStates[3] != 0)
			cmd << " U0";
		if (sslStates[4] != 0)
			cmd << " V0";
		if (sslStates[5] != 0)
			cmd << " W0";
	}
	cmd << "\n";
	m_Socket << cmd.str();
	CheckHexapodForError("Could not disable soft limits", __FUNCTION__);

}
// ============================================================================
// Get status of soft limits
// ============================================================================
void PIHexapod::GetSoftLimitEnabledStatus(std::vector<int>& sslStates)
{
	CheckConnection(__FUNCTION__);
	CheckStandby(__FUNCTION__);
	m_Socket << "SSL?\n";
	ReadValues(sslStates);
}

// ============================================================================
// utility to read 6 integer values from hexapod
// ============================================================================
void PIHexapod::ReadValues(std::vector<int>& values)
{
	values.clear();
	string answer;
	do
	{
		m_Socket >> answer;
		istringstream is(answer);
		string line;
		int val;
		while(getline(is, line))
		{
			size_t p = line.find('=');
			if (p == string::npos)
			{
				throw yat::Exception(	"SOFTWARE_ERROR",
										"unexpected response to query",
										__FUNCTION__ );
			}
			istringstream spos(line.substr(p+1));
			spos >> val;
			values.push_back(val);
		}
	} while (values.size()<6);
}

// ============================================================================
// Ask hexapod if given target is reachable
// ============================================================================
bool PIHexapod::IsTargetReachable(const std::vector<double>& target)
{
	CheckConnection(__FUNCTION__);
	if (target.size() != 6)
	{
		throw yat::Exception(	"SOFTWARE_ERROR",
								"Invalid number of target positions (must be 6)",
								__FUNCTION__ );
	}
	CheckStandby(__FUNCTION__);
	ostringstream cmd;
	if (conf.version810)
	{
		cmd	<< "VMO" 
			<< " X " << target[0]
			<< " Y " << target[1]
			<< " Z " << target[2]
			<< " U " << target[3]
			<< " W " << target[4]
			<< " W " << target[5]
			<<"\n";
	}	
	else
	{
		cmd	<< "VMO?" 
			<< " X " << target[0]
			<< " Y " << target[1]
			<< " Z " << target[2]
			<< " U " << target[3]
			<< " W " << target[4]
			<< " W " << target[5]
			<<"\n";
	}	

	m_Socket << cmd.str();
	std::string answer;
	m_Socket >> answer;
	istringstream is(answer);
	int response;
	is >> response;
	if (conf.version810)
	{
		return (response == 0);
	}
	else
	{
		return (response == 1);		
	}
}
