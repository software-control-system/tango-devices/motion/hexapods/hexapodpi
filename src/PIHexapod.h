//- this should not be necessary, included in ClientSocket or deeper includes
//- #include <yat/network/Address.h>
#include <yat/network/ClientSocket.h>

class PIHexapod
{

public:
  //- the configuration structure
  typedef struct Config
  {
    //- members
    std::string url;
    size_t port;
    bool version810;

    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- Ctor -------------
    Config (std::string url );

    //- operator = -------
    void operator = (const Config & src);
  }Config;



	PIHexapod(Config & conf);
	virtual ~PIHexapod();


	typedef enum
	{
		NOT_CONNECTED,   ///< Connect() has not been called, Hexapod controller was not found or Disconnect() has been called
    CONNECTED,       //- TCP connection done
    CONNECTION_ERROR,//- Failed to connect to PI Hexapod
		NOT_REFERENCED,  ///< hexapod is connected but not initialized/homed/referenced
		REFERENCING,     ///< hexapod performing homing /referencing motions
		STANDBY,         ///< Hexapod initialized and ready for motion commands
		MOVING,          ///< hexapod on its way to the new target position
		SERVO_OFF,       ///< 
		MOTION_ERROR,
		HEXPOD_ERROR
	} HEXAPOD_STATE;

	enum
	{
		HEXAPOD_MOTION_ERROR = 0x00003F,
		HEXAPOD_MOVING = 0x003F00,//810
		HEXAPOD_REFERENCING = 0x080000,		
		HEXAPOD_READY = 0x010000,
		HEXAPOD_MOTION = 0x3F//811
	};

	HEXAPOD_STATE GetStatus();

	void Home(); ///< start referencing

	void Stop(); ///< stop anything, reference, normal motion

	double GetXPos(); ///< current position of X-Axis
	double GetYPos(); ///< current position of Y-Axis
	double GetZPos(); ///< current position of Z-Axis
	double GetUPos(); ///< current position of U-Axis
	double GetVPos(); ///< current position of V-Axis
	double GetWPos(); ///< current position of W-Axis
	void GetPosition(std::vector<double>& positions); ///< get current positions of XYZUVW

	bool IsTargetReachable(const std::vector<double>& target); ///< check target position without performing any motion

	void Move(const std::vector<double>& target);///< move to target position

	double GetXTarget() { return lastTarget[0]; } ///< current target of X-Axis
	double GetYTarget() { return lastTarget[1]; } ///< current target of Y-Axis
	double GetZTarget() { return lastTarget[2]; } ///< current target of Z-Axis
	double GetUTarget() { return lastTarget[3]; } ///< current target of U-Axis
	double GetVTarget() { return lastTarget[4]; } ///< current target of V-Axis
	double GetWTarget() { return lastTarget[5]; } ///< current target of W-Axis
	void GetTarget(std::vector<double>& targets) { targets = lastTarget; } ///< get current target of XYZUVW

	void SetVelocity(double vel);///< set system velocity
	double GetVelocity();///< get system velocity

	void EnableServo();///< switch on servo control, i.e. start servo control loop
	void DisableServo();///< switch off servo control, i.e. stop servo control loop

  //- i presume that this is a way to send ASCII commands to the motion controller
	void SendGCSCommand(const std::string& command);///< send GCS commmand to controller
	std::string ReadGCSAnswer();///< read answer from Controller

	// would it be better to have two function? One for positive and one for negative soft limits?
	void GetSoftLimits(std::vector<double>& positiveLimits, std::vector<double>& negativeLimits); ///< get current soft limits for XYZUVW
	void SetSoftLimits(const std::vector<double> positiveLimits, const std::vector<double> negativeLimits); ///< set current soft limits for XYZUVW

  //- i would prefer this way 
  void EnableSoftLimits (const std::vector<int>& sslStates); //- enables the soft limits thare are marked "true" (1)
  void DisableSoftLimits (const std::vector<int>& sslStates);//- disables the soft limits thare are marked "true" (1)
	void GetSoftLimitEnabledStatus(std::vector<int>& sslStates);///< get state of soft limits for XYZUVW

  //- get/set the pivot point coordinates
  std::vector <double> GetPivotPointCoordinates (void);
  void SetPivotPointCoordinates (std::vector <double> positions);


protected:
	HEXAPOD_STATE m_State;
  //- the socket
	yat::ClientSocket m_Socket;
  //- the class configuration (mainly socket configuration)
  Config conf;
  //- the last used target (hexapod controller does not store target)
  std::vector<double> lastTarget;
	void Connect(Config conf); 
	void Disconnect();
  HEXAPOD_STATE ReadStateFromHexapod811();
	HEXAPOD_STATE ReadStateFromHexapod();

	void InitSocket();
	int ReadError();
	void CheckConnection(const char* funcname)
	{
		if (m_State == NOT_CONNECTED || m_State == CONNECTION_ERROR)
		{
			throw yat::Exception(	"SOFTWARE_ERROR", 
				                    "Connection with Hexapod not opened or broken",
								    funcname);
		}
	}
	void CheckBusy(const char* funcname)
	{
		if (m_State == MOVING || m_State == REFERENCING)
		{
			m_State = ReadStateFromHexapod();
			if (m_State == MOVING || m_State == REFERENCING)
			{
				throw yat::Exception(	"OPERATION_NOT_ALLOWED", 
					                    "Hexapod is already homing/moving",
									    funcname);
			}
		}
	}

  //- JC : try to workaround the exception thrown on reading velocity while moving
  inline bool IsMoving (void)
  {
		if (m_State == MOVING || m_State == REFERENCING)
		{
			m_State = ReadStateFromHexapod();
      return true;
    }
    return false;
  }

	void CheckStandby(const char* funcname)
	{
		if (m_State != STANDBY)
		{
			m_State = ReadStateFromHexapod();
			if (m_State != STANDBY)
			{
				throw yat::Exception(	"OPERATION_NOT_ALLOWED", 
					                    "Hexapod is not ready for motion",
									    funcname);
			}
		}
	}
	void ThrowHexapodError(const std::string& description, const std::string& funcname);
	void CheckHexapodForError(const std::string& description, const std::string& funcname);
	void ThrowSoftwareError(const std::string& description, const std::string& funcname);
	void ReadPosition(std::vector<double>& positions);
	void ReadValues(std::vector<int>& values);
	double ReadSinglePosition(const std::string& axisName, int index);

  private : 
    //- internal velocity var to keep the velocity reading when moving
    double m_velocity;
};
